import cv2
import numpy as np
import pyzbar.pyzbar as pyzbar

cap = cv2.VideoCapture('/home/mai/zzz/vid1.mp4')
font = cv2.FONT_HERSHEY_PLAIN

while True:
    ret, frame = cap.read()

    decodedObj = pyzbar.decode(frame)
    
    for obj in decodedObj:
    	(x,y,width,height) = obj.rect
    	cv2.rectangle(frame, (x,y),(x+width,y+height),(255,0,0),2)
    	
    	info = obj.data.decode('utf-8')
    	print("Data: ", info)
    	cv2.putText(frame, str(info), (x, y-10), font, 2, (255, 0, 0), 3)

    cv2.imshow("Frame", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break
